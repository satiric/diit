<?php

use yii\db\Migration;

class m170503_040337_change_formula_in_project extends Migration
{
    public function up()
    {
        $this->alterColumn("projects","formula","mediumtext");
    }

    public function down()
    {
        echo "m170503_040337_change_formula_in_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
