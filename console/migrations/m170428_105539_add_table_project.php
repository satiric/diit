<?php

use yii\db\Migration;

class m170428_105539_add_table_project extends Migration
{
    public function up()
    {
        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'code' => $this->string(255)->notNull(),
            'title' => $this->string(255)->null(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

    }

    public function down()
    {
        echo "m170428_105539_add_table_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
