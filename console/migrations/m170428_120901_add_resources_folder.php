<?php

use yii\db\Migration;

class m170428_120901_add_resources_folder extends Migration
{
    public function safeUp()
    {
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'ext' => $this->string(255)->null(),
            'type' => $this->string(255)->notNull()->defaultValue('1'),
            'project_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk-files-project_id', 'files', 'project_id', 'projects', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m170428_120901_add_resources_folder cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
