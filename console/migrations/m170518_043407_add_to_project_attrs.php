<?php

use yii\db\Migration;

class m170518_043407_add_to_project_attrs extends Migration
{
    public function up()
    {
        $this->addColumn("projects", "attrs", "TEXT NULL DEFAULT NULL");

    }

    public function down()
    {
        $this->dropColumn("projects", "attrs");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
