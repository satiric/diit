<?php

use yii\db\Migration;

class m170428_113327_drop_code_project extends Migration
{
    public function up()
    {
        $this->dropColumn('projects', 'code');

    }

    public function down()
    {
        echo "m170428_113327_drop_code_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
