<?php

use yii\db\Migration;

class m170503_040043_add_formula_to_project extends Migration
{
    public function up()
    {
        $this->addColumn("projects","formula", "VARCHAR(255) NULL DEFAULT NULL");
    }

    public function down()
    {
        $this->dropColumn("projects", "formula");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
