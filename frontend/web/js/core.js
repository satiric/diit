// Structures with Alphabet

var genNodes = [];

function getHtmlInput(val) {
    return ' <div class="input-group">\
        <input type="text" class="form-control" value="'+ val +'">\
        <span class="input-group-btn">\
        <button title="Удалить" class="btn btn-default removeRule" type="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>&nbsp;</button>\
    </span>\
    </div>';
}

function getHtmlRules(rules) {
    var html = '';
    for(var i = 0, size = rules.length; i < size; i++) {
        var cur = rules[i];
        html += getHtmlInput(cur);
    }
    return html;
}

function genRandom(min, max) {
    return ((Math.random() * (max - min) + min)).toFixed(2);
}

function getBtnPlus() {
    return '<a class="btn btn-primary addRule" href="#" role="button" aria-haspopup="true" aria-expanded="false"> ' +
        '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </a>';
}

function getBtnSave() {
    return '<a class="btn btn-primary saveRule" href="#" role="button" aria-haspopup="true" aria-expanded="false"> ' +
        '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> </a>';
}

function fileInput() {
    // var lexem = $(this).parents('tr').find('.captCont').html();
    // console.log(this);
    // for (var id in lexemsData ) {
    //     if(!lexemsData.hasOwnProperty(id)) {
    //         continue;
    //     }
    //
    //     if(lexemsData[id]['caption'] == lexem) {
    //
    //     }
    // }
}

function getBtnImage( text ) {
    text = text || '';
return '<button class="btn btn-primary btn-sm load-img" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#sendImg"> \
    <span class="glyphicon glyphicon-picture" aria-hidden="true"></span> ' + text +
'</button>';
}


function setNotSaveState($save) {
    if($save.hasClass('btn-primary')) {
        $save.removeClass('btn-primary');
        $save.addClass('btn-default');
    }
}
function setSaveState($save) {
    if($save.hasClass('btn-default')) {
        $save.removeClass('btn-default');
        $save.addClass('btn-primary');
    }
}

function changeInput() {
    var $line = $(this).parents('tr'),
        $capt = $line.find('.captCont'),
        $rule = $line.find('.ruleCont'),
        $rules = $rule.find('input'),
        caption = $capt.html(),
        i = 0,
        $save = $line.find('.saveRule');
    $rules.each(function () {
        if (SyntaxAnalizer._ents[caption]["rules"][i] != $(this).val()) {
            setNotSaveState($save);
        }
        i++;
    });
}
function getBtnFile() {
    return '';
}

function getControl() {
    return getBtnPlus()+getBtnSave()+getBtnFile();
}

function removeRuleClick() {
    var $line = $(this).parents('tr');
    var $save = $line.find('.saveRule');
    $(this).parents(".input-group").remove();
    setNotSaveState($save);
}

function getRateInput() {
    return '<input class="isReciver" type="checkbox">';
}

function createEntLine(ent) {
    return "<tr><td class='captCont'>"+ent+"</td><td class='ruleCont'>"+ getHtmlRules(entities.getRules(ent))+"</td>" +
            "<td>" + getRateInput(ent) + "</td>" +
        "<td>" + getBtnImage() + "</td>" +
        "<td>" + getControl() +"</td></tr>";
}

function bindHandlers($container) {

    $container.find('.addRule').on('click',function(e){
        e.preventDefault();
        var $line = $(this).parents('tr');
        var $save = $line.find('.saveRule');
        var $rule = $line.find('.ruleCont');
        $rule.append(getHtmlInput(''));
        $rule.find('input').off('change');
        $rule.find('input').on('change', changeInput);
        $line.find('.removeRule').off('click');
        $line.find('.removeRule').on('click',removeRuleClick);
        setNotSaveState($save);
    });

    $container.find('.removeRule').on('click',removeRuleClick);
    $container.find('.ruleCont input').on('change', changeInput);

    $container.find('.saveRule').click(function(e){
        e.preventDefault();
        var $line = $(this).parents('tr'),
            $capt = $line.find('.captCont'),
            $rule = $line.find('.ruleCont'),
            $rules = $rule.find('input'),
            $save = $line.find('.saveRule'),
            contRules = [],
            caption = $capt.html();
        $rules.each(function(){
            contRules[contRules.length] = $(this).val();
        });
        SyntaxAnalizer._ents[caption]["rules"] = contRules;

        $.ajax({
            method: 'POST',
            url: '/rules/update-all',
            data: {
                'lexem_id': lexemIds[caption],
                'rules': contRules
            },
            success : function(resp) {
                console.log(resp);
                if (resp.status =='success') {
                    setSaveState($save);
                }
                else {
                    alert ('db error');
                }
            }
        });
    });
}

function generValue(min, max) {
    return (Math.random() * (max - min) + min);

};
function createSchemeSubject(template, name) {
    
    var attrs = [];
    if(!template.attrs) {
        return {
            'name' : name,
            'is_recive': template['is_recive'] || false,
            'attrs' : []
        }
    }
    for (var i in template.attrs) {
        if(!template.attrs.hasOwnProperty(i)) {
            continue;
        }
        var cur = template.attrs[i];
        var val = generValue(cur.min_value, cur.max_value);
        var sub = {
            'caption': cur.caption,
            'value': Number(val).toPrecision(4),
            'min_value': cur.min_value,
            'max_value': cur.max_value,
            'symbol' : cur.symbol
        };
        attrs.push(sub);
    }

    return {
        'name' : name,
        'is_recive': template['is_recive'],
        'attrs' : attrs
    }
}

var Renderer = function(canvas){
    var canvas = $(canvas).get(0);
    if(!canvas) {
        return;
    }
    var ctx = canvas.getContext("2d");
    var particleSystem;

    var that = {
        init:function(system){
            //
            // the particle system will call the init function once, right before the
            // first frame is to be drawn. it's a good place to set up the canvas and
            // to pass the canvas size to the particle system
            //
            // save a reference to the particle system for use in the .redraw() loop
            particleSystem = system;

            // inform the system of the screen dimensions so it can map coords for us.
            // if the canvas is ever resized, screenSize should be called again with
            // the new dimensions
            particleSystem.screenSize(canvas.width, canvas.height);
            particleSystem.screenPadding(80); // leave an extra 80px of whitespace per side

            // set up some event handlers to allow for node-dragging
            that.initMouseHandling();
        },

        redraw:function(){
            //
            // redraw will be called repeatedly during the run whenever the node positions
            // change. the new positions for the nodes can be accessed by looking at the
            // .p attribute of a given node. however the p.x & p.y values are in the coordinates
            // of the particle system rather than the screen. you can either map them to
            // the screen yourself, or use the convenience iterators .eachNode (and .eachEdge)
            // which allow you to step through the actual node objects but also pass an
            // x,y point in the screen's coordinate system
            //
            ctx.fillStyle = "white";
            ctx.fillRect(0,0, canvas.width, canvas.height);

            particleSystem.eachEdge(function(edge, pt1, pt2){
                // edge: {source:Node, target:Node, length:#, data:{}}
                // pt1:  {x:#, y:#}  source position in screen coords
                // pt2:  {x:#, y:#}  target position in screen coords

                // draw a line from pt1 to pt2
                ctx.strokeStyle = "rgba(0,0,0, .333)";
                ctx.lineWidth = 1;
                ctx.beginPath();
                ctx.moveTo(pt1.x, pt1.y);
                ctx.lineTo(pt2.x, pt2.y);
                ctx.stroke();
            })

            particleSystem.eachNode(function(node, pt){
                // node: {mass:#, p:{x,y}, name:"", data:{}}
                // pt:   {x:#, y:#}  node position in screen coords

                // draw a rectangle centered at pt
                var w = 30;
                // ctx.fillStyle = getColor(node.data.literal);//(node.data.alone) ? "orange" : "black"
                // ctx.fillRect(pt.x-w/2, pt.y-w/2, w,w);


                var src = node.data.src;
                if(typeof src =='undefined') {
                    // ctx.strokeStyle = "rgb(255,0,0)";
                    // ctx.rect(pt.x-w/2, pt.y-2,pt.x-w/2 + 2,pt.y)
                    // ctx.stroke();

                } else {
                    var img = new Image;
                    img.src = src;

                    ctx.drawImage(img,pt.x-w/2, pt.y-w/2 -20, w,w);
                }


                var w = ctx.measureText(node.data.label||"").width + 6;
                var label = node.data.label;
                
                if (!(label||"").match(/^[ \t]*$/)){
                    pt.x = Math.floor(pt.x);
                    pt.y = Math.floor(pt.y);
                }else{
                    label = null
                }

                // clear any edges below the text label
                // ctx.fillStyle = 'rgba(255,255,255,.6)'
                // ctx.fillRect(pt.x-w/2, pt.y-7, w,14)


                ctx.clearRect(pt.x-w/2, pt.y+5, w,14)
                if (label){
                    ctx.font = "bold 11px Arial";
                    ctx.textAlign = "center";

                    // if (node.data.region) ctx.fillStyle = palette[node.data.region]
                    // else ctx.fillStyle = "#888888"
                    ctx.fillStyle = "#888888";

                    // ctx.fillText(label||"", pt.x, pt.y+4)
                    ctx.fillText(label||"", pt.x, pt.y+16);
                }
            })
        },

        initMouseHandling:function(){
            // no-nonsense drag and drop (thanks springy.js)
            var dragged = null;

            // set up a handler object that will initially listen for mousedowns then
            // for moves and mouseups while dragging
            var handler = {
                clicked:function(e){
                    var pos = $(canvas).offset();
                    _mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top);

                    dragged = particleSystem.nearest(_mouseP);
                    if (dragged && dragged.node !== null){
                        var $panel = $('.' +dragged.node.data.label);
                        $panel.siblings().removeClass('panel-primary');
                        $panel.addClass('panel-primary');
                        // while we're dragging, don't let physics move the node
                        dragged.node.fixed = true
                    }

                    $(canvas).bind('mousemove', handler.dragged)
                    $(window).bind('mouseup', handler.dropped)

                    return false
                },
                dragged:function(e){
                    var pos = $(canvas).offset();
                    var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)

                    if (dragged && dragged.node !== null){
                        var p = particleSystem.fromScreen(s)
                        dragged.node.p = p
                    }

                    return false
                },

                dropped:function(e){
                    if (dragged===null || dragged.node===undefined) return
                    if (dragged.node !== null) dragged.node.fixed = false
                    dragged.node.tempMass = 1000
                    dragged = null
                    $(canvas).unbind('mousemove', handler.dragged)
                    $(window).unbind('mouseup', handler.dropped)
                    _mouseP = null
                    return false
                }
            }

            // start listening
            $(canvas).mousedown(handler.clicked);

        },

    }
    return that
}


$(document).ready(function(){
    $("#saveOtherSchemeImage").on('click', function(){
        var pid = $(this).data('pid');
        console.log(pid);
        $("#input-44").fileinput({
            'dropZoneTitle': 'Перетягніть зображення сюди...',
            language : 'ru',
            'allowedFileExtensions' : ['png'],
            uploadUrl: '/projects/load-file/' + pid,
            maxFilePreviewSize: 10240
        });
        $("#input-44").off('fileuploaded');
        $("#input-44").on('fileuploaded', function(event, data, previewId, index) {
         //   response = data.response;
       //     lexemsData[lexemId]['image'] = response.file;
            $('#sendImg').modal('hide');
            $('#input-44').fileinput('destroy');
            location.reload();
        });
    });

    var sendAttrApp = new Vue({
        'el': '#sendAttr',
        data : {
            attr: {
                'id' : '',
                'caption' : '',
                'symbol' : '',
                'max_value': '',
                'min_value' : '',
                'id_lexem' : '',
            }
        }
    });

    console.log(savedProjectAttrs);
    var workPanel = new Vue({
        'el': '#workPanel',
        data : {
            subjectsList: (savedProjectAttrs) ? savedProjectAttrs : [subjects ]
        },
        methods: {
            subm: function() {
                var $sols =  $("#solutions");
                console.log(savedProjectAttrs);
                console.log('-------');
                var arr = $sols.serializeArray();
                $.post(
                    {
                        'url':'/projects/save-experience/'+selectedProject,
                        data: {
                            attrs: JSON.stringify(savedProjectAttrs),
                            solution: arr
                        },
                        success: function(resp) {
                            $sols[0].reset();
                            $("#genVals").click();

                        }
                    }
                );



            }



        }
    });


    var sendRuleApp = new Vue({
        'el': '#sendRule',
        data : {
            rule: {
                'id' : '',
                'body' : '',
                'chance' : '',
                'id_lexem' : '',
            }
        }
    });


    var sendLexemApp = new Vue({
        'el': '#sendLexem',
        data : {
            lexem: {
                'id' : '',
                'name' : '',
                'image' : '',
                'is_recive' : '',
                'attrs' : [],
                'rules' : [],
            }
        }
    });


    var appLexems = new Vue({
        el: '#logsTable',
        data: {
            lexems: lexemsData
        },
        methods: {
            setActive: function (rule) {
                sendRuleApp.rule = rule;
            },
            setActiveLexem: function (lexem) {
                sendLexemApp.lexem = lexem
            },
            loadImage: function(lexemId) {
                $("#input-44").fileinput({
                    'dropZoneTitle': 'Перетягніть зображення сюди...',
                    language : 'ru',
                    'allowedFileExtensions' : ['jpg', 'jpeg', 'png','gif'],
                    uploadUrl: '/site/load-file/' + lexemId,
                    maxFilePreviewSize: 10240,
                });
                $("#input-44").off('fileuploaded');
                $("#input-44").on('fileuploaded', function(event, data, previewId, index) {
                    response = data.response;
                    lexemsData[lexemId]['image'] = response.file;
                    $('#sendImg').modal('hide');
                    $('#input-44').fileinput('destroy');
                });
            },
            removeAttr: function (id) {
                $.ajax({
                    method: 'POST',
                    url: '/attrs/delete/'+id,
                    success: function() {
                        location.reload();
                    }
                });

            }
        }
    });

    var appAttrs = new Vue({
        el: '#attrs',
        data: {
            lexems: lexemsData
        },
        methods: {
            setActive: function (attrId, lexemId) {
                var attrs = this.lexems[lexemId].attrs;
                for (var i in attrs) {
                    if (attrs[i].id == attrId) {
                        sendAttrApp.attr = attrs[i];
                    }
                }
            },
            removeAttr: function (id) {
                $.ajax({
                    method: 'POST',
                    url: '/attrs/delete/'+id,
                    success: function() {
                        location.reload();
                    }
                });
            }
        }
    });

    var appSubjects = new Vue({
        el: '#statusPanel',
        data: {
            'pos' : 1,
            subjectsList: (savedProjectAttrs) ? savedProjectAttrs : [subjects ]
        },
        methods: {
            generate: function() {

                this.subjectsList = this.subjectsList.map(function(val){
                    val.attrs = val.attrs.map(function(attr) {
                        attr.value = genRandom(parseFloat(attr.min_value), parseFloat(attr.max_value));
                        return attr;
                    });
                    return val;
                });
            }
        }
    });

    $('.edit-lexem').click(function(){
        sendAttrApp.attr.id_lexem = $(this).data('id');
    });

    $("#sendAttr form").submit(function(){
        var id = $(this).find('input[name=id_lexem]').val();
        var $self = $(this);
        var formdata = $(this).serializeArray();
        var data = {};
        var $close = $(this).find('.closer');
        $(formdata ).each(function(index, obj){
            data[obj.name] = obj.value;
        });
        $.ajax({
            data: $(this).serialize(),
            method: 'POST',
            url: '/attrs/create',
            success: function() {
                console.log(id);
                lexemsData[id].attrs.push(data);
                $self.trigger( 'reset' );
                $close.click();

            }
        });
        return false;
    });

    $("#sendLexem form").submit(function(){
        var id = $(this).find('input[name=id]').val();
        var val = $(this).find('input[name=is_recive]').val();
        var $self = $(this);

        var formdata = $(this).serializeArray();
        var data = {};
        var $close = $(this).find('.closer');
        $(formdata ).each(function(index, obj){
            data[obj.name] = obj.value;
        });
        console.log($(this).serialize());
        $.ajax({
            data: $(this).serialize(),
            method: 'POST',
            url: '/lexem/create',
            success: function(res) {
                console.log(lexemsData[id]);
                console.log(res);
                lexemsData[id].name = data.name;
                lexemsData[id].is_recive = data.is_recive == 'on';
                $self.trigger( 'reset' );
                $close.click();

            }
        });
        return false;
    });


    $("#sendRule form").submit(function(){
        var id = $(this).find('input[name=id_lexem]').val();
        var id_rule = $(this).find('input[name=id]').val();
        var val = $(this).find('input[name=is_recive]').val();
        var $self = $(this);

        var formdata = $(this).serializeArray();
        var data = {};
        var $close = $(this).find('.closer');
        $(formdata ).each(function(index, obj){
            data[obj.name] = obj.value;
        });
        console.log($(this).serialize());
        $.ajax({
            data: $(this).serialize(),
            method: 'POST',
            url: '/rules/create',
            success: function(res) {
                if(lexemsData[id]) {
                    for (var i in lexemsData[id]['rules']) {
                        if (lexemsData[id]['rules'][i]['id'] == id_rule ) {
                            lexemsData[id].rules[i] = data;
                       //     console.log('--------');
                       //     console.log(lexemsData[id]['rules']);
                            break;
                        }
                    }
                }

                $self.trigger( 'reset' );
                $close.click();
                // console.log(lexemsData[id]['attrs']);
                // console.log(res);

                // lexemsData[id].name = data.name;
                // lexemsData[id].is_recive = data.is_recive;



            }
        });
        return false;
    });


    for (var key in lexemArray) {
        if(!lexemArray.hasOwnProperty(key)) {
            continue;
        }
        SyntaxAnalizer.entityAdd(key, lexemArray[key]);
    }

    var sys = arbor.ParticleSystem(0, 0, 0 ); // create the system with sensible repulsion/stiffness/friction
    sys.parameters({gravity:false}); // use center-gravity to make the graph settle nicely (ymmv)
    sys.renderer = Renderer("#viewport"); // our newly created renderer will have its .init() method called shortly by sys...

    // var html = '';
    // for(ent in entities)
    // {
    //     if(!entities.hasOwnProperty(ent) || ent === "getRules") {
    //         continue;
    //     }
    //     html+= createEntLine(ent);
    //
    // }
    // var $content = $("#logsTable tbody");
    // $content.html(html);
    // bindHandlers($content);

    $(".addEntity").click(function(e){
        e.preventDefault();
        var name = prompt("Введіть літерал", '');
        if(name && SyntaxAnalizer.entityAdd(name)) {
            $.ajax({
                method: 'POST',
                url: '/lexem/create',
                data: {
                    'name': name,
                    'caption': name,
                },
                success : function(resp) {
                    console.log(resp);
                    if (resp.status =='success') {
                        $content.append(createEntLine(name));
                        bindHandlers( $content.find('tr:last-child'));   
                    }
                    else {
                        alert ('db error');
                    }
                }
            });
        }
    });

    $("#StateTabLink").click(function(){
        $("#stateResult").html(JSON.stringify(SyntaxAnalizer, null, 2));
    });

    $('.buildScheme').click(function(){
        $('.saveSchemeImage').fadeIn();
        $(this).fadeOut();
        $(".bootstrapScheme").fadeOut();
        var $schemeInpt = $("#schemeCode");
        $schemeInpt.attr('type','hidden');
        var schemeCode = $schemeInpt.val();
        $schemeInpt.before("<p>"+ schemeCode + "</p>")

        try {
            SyntaxAnalizer.buildScheme(schemeCode, sys, appSubjects);
            workPanel.subjectsList = appSubjects.subjectsList;
        }
        catch(e) {
            $("#errorScheme").html(e);
            return;
        }
        $("#errorScheme").html('');
        if(!selectedProject) {
            throw "Проект не знайдено";
            return;
        }
        console.log('--');
        console.log(schemeCode);
        $.ajax({
            'url':'/projects/bind-scheme/'+selectedProject,
            'method':'POST',
            'data': {
                'formula': schemeCode
            },
            'success' : function (resp) {

            },
            'error' : function (resp) {
                console.log(resp);
                alert('error');
            }
        });
    });

    $('.bootstrapScheme').click(function(){
        var schemeCode = $('#schemeCode').val();
        try {
            var result = SyntaxAnalizer.bootstrapString(schemeCode);
        }
        catch(e) {
            $("#errorScheme").html(e);
            return;
        }
        $('#schemeCode').val(result);
        $("#errorScheme").html('');
    });
    $('.saveSchemeImage').click(function(){
        var canvas = $("#viewport").get(0);
        $.ajax({
            type: "POST",
            url: "/projects/bind-scheme-image/"+selectedProject,
            data: {
                file: canvas.toDataURL('png'), 
                collected: JSON.stringify(window.collect)
            }
        }).done(function(o) {
            
            
            location.reload();
            // If you want the file to be visible in the browser
            // - please modify the callback in javascript. All you
            // need is to return the url to the file, you just saved
            // and than put the image in your browser.
        });


    });


    $('#contentTabs li').click(function(){
        if($("#scheme-image").length !=0) {
            if($(this).attr('id') !== 'Statistic') {
                $('#statusPanel').fadeOut(200);
                $('#workPanel').fadeOut(200);
                $("#contentLogs").removeClass('col-md-7');
                $("#contentLogs").addClass('col-md-offset-2');
                $("#contentLogs").addClass('col-md-9');
            }
            else {

                    $('#statusPanel').fadeIn(200);
                    $('#workPanel').fadeIn(200);
                    $("#contentLogs").removeClass('col-md-9');
                    $("#contentLogs").addClass('col-md-7');
                    $("#contentLogs").removeClass('col-md-offset-2');



            }
        }
    });

    $("#Statistic a").click();
});

//sys.addNode
// (function(){
//
//     Renderer = function(canvas){
//         canvas = $(canvas).get(0)
//         var ctx = canvas.getContext("2d")
//         var particleSystem = null
//
//         var palette = {
//             "Africa": "#D68300",
//             "Asia": "#4D7A00",
//             "Europe": "#6D87CF",
//             "North America": "#D4E200",
//             "Oceania": "#4F2170",
//             "South America": "#CD2900"
//         }
//
//         var that = {
//             init:function(system){
//                 particleSystem = system
//                 particleSystem.screen({padding:[100, 60, 60, 60], // leave some space at the bottom for the param sliders
//                     step:.02}) // have the â€˜cameraâ€™ zoom somewhat slowly as the graph unfolds
//                 $(window).resize(that.resize)
//                 that.resize()
//
//                 that.initMouseHandling()
//             },
//             redraw:function(){
//                 if (particleSystem===null) return
//
//                 ctx.clearRect(0,0, canvas.width, canvas.height)
//                 ctx.strokeStyle = "#d3d3d3"
//                 ctx.lineWidth = 1
//                 ctx.beginPath()
//                 particleSystem.eachEdge(function(edge, pt1, pt2){
//                     // edge: {source:Node, target:Node, length:#, data:{}}
//                     // pt1:  {x:#, y:#}  source position in screen coords
//                     // pt2:  {x:#, y:#}  target position in screen coords
//
//                     var weight = null // Math.max(1,edge.data.border/100)
//                     var color = null // edge.data.color
//                     if (!color || (""+color).match(/^[ \t]*$/)) color = null
//
//                     if (color!==undefined || weight!==undefined){
//                         ctx.save()
//                         ctx.beginPath()
//
//                         if (!isNaN(weight)) ctx.lineWidth = weight
//
//                         if (edge.source.data.region==edge.target.data.region){
//                             ctx.strokeStyle = palette[edge.source.data.region]
//                         }
//
//                         // if (color) ctx.strokeStyle = color
//                         ctx.fillStyle = null
//
//                         ctx.moveTo(pt1.x, pt1.y)
//                         ctx.lineTo(pt2.x, pt2.y)
//                         ctx.stroke()
//                         ctx.restore()
//                     }else{
//                         // draw a line from pt1 to pt2
//                         ctx.moveTo(pt1.x, pt1.y)
//                         ctx.lineTo(pt2.x, pt2.y)
//                     }
//                 })
//                 ctx.stroke()
//
//                 particleSystem.eachNode(function(node, pt){
//                     // node: {mass:#, p:{x,y}, name:"", data:{}}
//                     // pt:   {x:#, y:#}  node position in screen coords
//                     // determine the box size and round off the coords if we'll be
//                     // drawing a text label (awful alignment jitter otherwise...)
//                     var w = ctx.measureText(node.data.label||"").width + 6
//                     var label = node.data.label
//                     if (!(label||"").match(/^[ \t]*$/)){
//                         pt.x = Math.floor(pt.x)
//                         pt.y = Math.floor(pt.y)
//                     }else{
//                         label = null
//                     }
//
//                     // clear any edges below the text label
//                     // ctx.fillStyle = 'rgba(255,255,255,.6)'
//                     // ctx.fillRect(pt.x-w/2, pt.y-7, w,14)
//
//
//                     ctx.clearRect(pt.x-w/2, pt.y-7, w,14)
//
//
//
//                     // draw the text
//                     if (label){
//                         ctx.font = "bold 11px Arial"
//                         ctx.textAlign = "center"
//
//                         // if (node.data.region) ctx.fillStyle = palette[node.data.region]
//                         // else ctx.fillStyle = "#888888"
//                         ctx.fillStyle = "#888888"
//
//                         // ctx.fillText(label||"", pt.x, pt.y+4)
//                         ctx.fillText(label||"", pt.x, pt.y+4)
//                     }
//                 })
//             },
//
//             resize:function(){
//                 var w = $(window).width(),
//                     h = $(window).height();
//                 canvas.width = w; canvas.height = h // resize the canvas element to fill the screen
//                 particleSystem.screenSize(w,h) // inform the system so it can map coords for us
//                 that.redraw()
//             },
//
//             initMouseHandling:function(){
//                 // no-nonsense drag and drop (thanks springy.js)
//                 selected = null;
//                 nearest = null;
//                 var dragged = null;
//                 var oldmass = 1
//
//                 $(canvas).mousedown(function(e){
//                     var pos = $(this).offset();
//                     var p = {x:e.pageX-pos.left, y:e.pageY-pos.top}
//                     selected = nearest = dragged = particleSystem.nearest(p);
//
//                     if (selected.node !== null){
//                         // dragged.node.tempMass = 10000
//                         dragged.node.fixed = true
//                     }
//                     return false
//                 });
//
//                 $(canvas).mousemove(function(e){
//                     var old_nearest = nearest && nearest.node._id
//                     var pos = $(this).offset();
//                     var s = {x:e.pageX-pos.left, y:e.pageY-pos.top};
//
//                     nearest = particleSystem.nearest(s);
//                     if (!nearest) return
//
//                     if (dragged !== null && dragged.node !== null){
//                         var p = particleSystem.fromScreen(s)
//                         dragged.node.p = {x:p.x, y:p.y}
//                         // dragged.tempMass = 10000
//                     }
//
//                     return false
//                 });
//
//                 $(window).bind('mouseup',function(e){
//                     if (dragged===null || dragged.node===undefined) return
//                     dragged.node.fixed = false
//                     dragged.node.tempMass = 100
//                     dragged = null;
//                     selected = null
//                     return false
//                 });
//
//             },
//
//         }
//
//         return that
//     }
//
//     var Maps = function(elt){
//         var sys = arbor.ParticleSystem(4000, 500, 0.5, 55)
//         sys.renderer = Renderer("#viewport") // our newly created renderer will have its .init() method called shortly by sys...
//
//         var dom = $(elt)
//         var _links = dom.find('ul')
//
//         var _sources = {
//             nations:'Derived from Wikipediaâ€™s <a target="_blank" href="http://en.wikipedia.org/wiki/List_of_countries_and_territories_by_land_borders">List of countries and territories by land borders</a>',
//             states:'Derived from <a target="_blank" href="http://www.statemaster.com/graph/geo_lan_bou_bor_cou-geography-land-borders">Land borders by state</a>',
//             risk:'Derived from Garrett Robinsonâ€™s <a target="_blank" href="http://web.mit.edu/sp.268/www/risk.pdf">The Strategy of Risk</a>'
//         }
//
//         var _maps = {
//             usofa:{title:"United States", p:{stiffness:600}, source:_sources.states},
//             africa:{title:"Africa", p:{stiffness:300}, source:_sources.nations},
//             asia:{title:"Asia", p:{stiffness:500}, source:_sources.nations},
//             europe:{title:"Europe", p:{stiffness:300}, source:_sources.nations},
//             mideast:{title:"Middle East", p:{stiffness:500}, source:_sources.nations},
//             risk:{title:"Risk", p:{stiffness:400}, source:_sources.risk}
//         }
//
//         var that = {
//             init:function(){
//
//                 $.each(_maps, function(stub, map){
//                     _links.append("<li><a href='#/"+stub+"' class='"+stub+"'>"+map.title+"</a></li>")
//                 })
//                 _links.find('li > a').click(that.mapClick)
//                 _links.find('.usofa').click()
//                 return that
//             },
//             mapClick:function(e){
//                 var selected = $(e.target)
//                 var newMap = selected.attr('class')
//                 if (newMap in _maps) that.selectMap(newMap)
//                 _links.find('li > a').removeClass('active')
//                 selected.addClass('active')
//                 return false
//             },
//             selectMap:function(map_id){
//                 $.getJSON("maps/"+map_id+".json",function(data){
//                     // load the raw data into the particle system as is (since it's already formatted correctly for .merge)
//
//                     var nodes = data.nodes
//                     $.each(nodes, function(name, info){
//                         info.label=name.replace(/(people's )?republic of /i,'').replace(/ and /g,' & ')
//                     })
//
//                     sys.merge({nodes:nodes, edges:data.edges})
//                     sys.parameters(_maps[map_id].p)
//                     $("#dataset").html(_maps[map_id].source)
//                 })
//
//             }
//         }
//
//         return that.init()
//     }
//
//     $(document).ready(function(){
//
//         var mcp = Maps("#maps");
//
//     })
//
//
// })()