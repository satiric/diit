/**
 * Created by decadal on 11.02.17.
 */
window.collect = [];
/**
 * @type {string[]} operations array contain signs of operation.
 */
var operations = ["+",":","*","(",")"];
var subjects = {
    'name' : '',
    'attrs' : [
        {
            'caption': '',
            'value': ''
        }
    ]
};
/**
 * @type object with grammar rules for lexems
 */
var entities = {
    /**
     * @param sym string for checking lexem on existing some rules
     * @returns {*}
     */
    getRules: function(sym) {
        if(!this[sym] || !this[sym]["rules"])
        {
            throw {
                message: "Internal structure error: rules not found for symbol '" + sym + "'",
                code: 1
            }
        }
        return this[sym]["rules"];
    }
};

var visualStack = {
    _stack : [],
    _opsStack : [],
    _prepared : null,
    _op : null,
    checkEmptyStacks : function() {
        return !(this._stack.length || this._opsStack.length)
    },
    prepare : function(entity) {
        this._prepared = entity;
    },
    prepareOp : function (op) {
        this._op = op;
    },
    getPrepared : function() {
        return this._prepared;
    },
    getPreparedOp : function() {
        return this._op;
    },
    addToOpsStack : function (entity) {
        this._opsStack.push(entity);
    },

    resetPrepared : function() {
        this.addToOpsStack(this._op);
        this._op = null;
        this.addToStack(this._prepared);
        this._prepared = null;
    },

    clearStack : function() {
        this._stack = [];
    },
    addToStack: function(node) {
        this._stack.push(node);
    },
    getFromStack: function(count) {
        if(typeof count === 'undefined' || !count) {
            count = 1;
        }
        return this._stack.splice(-1, count);
    },

    getFromStackOp: function(count) {
        if(typeof count === 'undefined' || !count) {
            count = 1;
        }
        return this._opsStack.splice(-1, count);
    },

    createVisualElement : function(node) {
        return {
            'left' : node,
            'right' : node,
            'nodes' : [node]
        }
    },
    extendVisualElement : function(node, nodes) {
        for (var i = 0, size = nodes.length; i < size; i++) {
            node['nodes'].push(nodes[i]);
        }
        return node;
    }


};
var schemeStack = {
    _stack : {},
    _lexems : [],
    _ops : [],
    _scope : [],
    _last : null,
    addToStack : function(ent) {
        if(typeof this._stack[ent] == 'undefined') {
            this._stack[ent] = [];
        }
        var name = ent + this._stack[ent].length;
        this._stack[ent].push( name );
        return name;
    },
    clearStack : function() {
        this._stack = {};
    },
    getLast : function (ent) {
        if(typeof this._stack[ent] != 'undefined') {
            var ln = this._stack[ent].length;
            return this._stack[ent][ln-1];
        }
        return null;
    },
    _addedEntity : function(ent) {
        this._scope.push(ent);
        this._last = ent;
    },
    addOp: function(op) {
        this._ops.push(op);
        this._addedEntity({
            'type' : 'op',
            'value' : op
        });
    },
    addLexem: function(lexem) {
        this._lexems.push(lexem);
        this._addedEntity({
            'type' : 'lexem',
            'value' : lexem
        });
    },
    getLastAdded : function() {
        return this._last;
    },
    getFirst: function() {
        return null;
    }

};

/**
 * object with Syntax Analysing logic
 */
var SyntaxAnalizer = {
    // private fields
    _ents: entities,
    _ops: operations,
    _sys: null,
    _app: null,
    // for result of processing string
    history: '',

    entityAdd: function(ent, rules) {
        rules = rules || [];
        if(typeof this._ents[ent] === 'undefined') {
            this._ents[ent] = {
                "rules": rules
            };
            return true;
        }
        return false;
    },

    /**
     * @param lex
     * @param num integer number of rule
     * @returns {*}
     */
    getLexemRule: function (lex, num) {
        //if num is not set
        if (typeof num === 'undefined') {
            num = -1; //
        }
        var rules = this._ents.getRules(lex);
        if (rules.length === 0) {
            return false;
        }
        var ind = (num === -1)
            ? parseInt(Math.random() * (rules.length))
            : num;
        if (typeof rules[ind] === 'undefined') {
            throw "rule with number " + ind + " is not exists for lexem: " + lex
        }
        return rules[ind];
    },

    /**
     * @param str
     */
    bootstrapString: function (str) {
        var accum = '';
        this.history = '';

        for (var i = 0, size = str.length; i < size; i++) {
            var lastSym = (i == (size - 1));
            // checking on litera
            if (isLitera(str[i])) {
                accum += str[i];
                if(!lastSym) {
                    continue; // if litera, accumulate it and continue
                }
            }
            // if symbol isn't operation or space, this is wrong syntax
            if (!lastSym && !isOp(this._ops, str[i]) && str[i]!== ' ') {
                throw "Позиция:" + i + "; Неизвестный символ: " + str[i];
            }
            //if accumulate is not empty, check it
            if (accum) {
                if (isEntity(this._ents, accum)) { //if entity, get new rule
                    var rule = this.getLexemRule(accum);
                    if (rule === false) {
                        throw "Позиция:" + i + "; Не найдено правил для лексемы: " + accum;
                    }
                    this.history += this.getLexemRule(accum);
                    if(!lastSym) {
                        this.history += str[i];
                    }
                    accum = '';
                    continue;
                }
                //else accum contain unknown lexem, this is wrong syntax
                throw "Позиция:" + i + "; Незивестная лексема: " + accum;
            }
            this.history += str[i];
        }
        return this.history;
    },
    _getByCapt: function(lexemsData, accum) {
//        console.log('----------++++++++++');
  //      console.log(lexemsData);
    //    console.log(accum);

        for (var id in lexemsData ) {
            if(!lexemsData.hasOwnProperty(id)) {
                continue;
            }
            if(lexemsData[id]['name'] == accum) {
                return lexemsData[id];
            }
        }
    },

    _getNext: function(schemeStack, str, posStart) {
        posStart = posStart || 0;
        var accum = '';
        for (var i = posStart, size = str.length; i < size; i++) {
            var currentSym = str[i];
            if (currentSym == ' ') {
                continue;
            }
            if (isLitera(currentSym) ) {
                accum += currentSym;
                if(i == size - 1) { //if last symbol and it is litera
                    schemeStack.addLexem(accum);
                    return i + 1; //link to next position
                }
                continue;
            }
            if( ! isLitera(currentSym) && accum ) { // not litera, but has accum
                schemeStack.addLexem(accum);
                return i;
            }
            if(isOp(this._ops, currentSym)) { //is operation
                schemeStack.addOp(currentSym);
                return i + 1; //link to next position
            }
            throw "Позиция:" + i + ";Неизвестный символ:" + currentSym;
        }

        return -1;
    },

    _renderLexem : function (lexem) {//, coordsX, coordsY
        var name;
        var subjName = lexem + this._app.pos;
        var nodeInfo = {
            'literal': lexem,
            'label': subjName,
            //'fixed':true
        };
        // if(typeof coordsX !== 'undefined') {
        //     nodeInfo['x'] = coordsX;
        // }
        // if(typeof coordsY !== 'undefined') {
        //     nodeInfo['y'] = coordsY;
        // }
        var lexemObj = this._getByCapt(lexemsData, lexem);
        // console.log('lexemsData');
        // console.log(lexemsData);
        // console.log('lexem');
        // console.log(lexem);
        window.collect.push(createSchemeSubject(lexemObj, subjName));
        Vue.set(this._app.subjectsList, this._app.pos, window.collect);
        nodeInfo["src"] = '/images/' + lexemObj['image'];
        this._app.pos++;
        name = schemeStack.addToStack(lexem);
        return this._sys.addNode(name, nodeInfo);// add some nodes to the graph and watch it go...
        // if (lastLexem) {
        //     var last = schemeStack.getLast(lastLexem);
        //     name = schemeStack.addToStack(accum);
        //     sys.addNode(name, nodeInfo);
        //     sys.addEdge( last, name,{'label': subjName });
        // }
        // else {
        //     name = schemeStack.addToStack(accum);
        //     sys.addNode(name, nodeInfo);// add some nodes to the graph and watch it go...
        // }
    },
    _createLine: function(firstNode, secondNode) {
        this._sys.addEdge(firstNode['right'], secondNode['left']);
        return {'left':firstNode['left'], 'right':secondNode['right']};

    },
    _createParallel: function(firstNode, secondNode) {

        var sys = this._sys;

        var curNode = firstNode['right'] ;
        var x = curNode.p.x;
        var y = firstNode['right'].p.y;
        var point1 = sys.addNode( (new Date()).getTime() + Math.random(), {'x':x-1, 'y':y+1});
        var point2 = sys.addNode((new Date()).getTime()+ Math.random(), {'x':x+1, 'y':y+1});
        // var point3 = sys.addNode(( (new Date()).getTime())+ Math.random(), {'x':x+1, 'y':y+1});
        // var point4 = sys.addNode(( (new Date()).getTime())+ Math.random(), {'x':x, 'y':y-1});
        // var point5 = sys.addNode(( (new Date()).getTime())+ Math.random(), {'x':x+0.5, 'y':y-1});
        // var point6 = sys.addNode(( (new Date()).getTime())+ Math.random(), {'x':x+1, 'y':y-1});

        sys.addEdge(firstNode['right'], point1);
        sys.addEdge(point1, secondNode['left']);
        sys.addEdge(firstNode['left'], point2);
        sys.addEdge(point2, secondNode['right']);
        // sys.addEdge(firstNode['right'], point1);
        // sys.addEdge(point1, point2);
        // sys.addEdge(point2, point3);
        // sys.addEdge(point3, secondNode['left']);
        // sys.addEdge(firstNode['right'], point4);
        // sys.addEdge(point4, point5);
        // sys.addEdge(point5, point6);
        // sys.addEdge(point6, secondNode['left']);
        return {'right':point1, 'left': point2};
    },
    _createExploded: function (firstNode, secondNode) {
        var sys = this._sys;
        // console.log('------');
        // console.log(firstNode);
        // console.log(secondNode);
        // console.log('------');
        var point1 = sys.addNode((new Date()).getTime()+ Math.random());
        sys.addEdge(firstNode['right'], point1);
        sys.addEdge(point1, secondNode['left']);
        return {'right':point1, 'left':point1};
    },


    _relateGroups : function(firstNode, secondNode, op) {
        switch(op) {
            case '+':
                return this._createLine(firstNode, secondNode);

            case ':':
                return this._createParallel(firstNode, secondNode);

            case '*':
                return this._createExploded(firstNode, secondNode);

        }

        // var lexemObj = this._getByCapt(lexemsData, first.value);
        // var subjName = accum + app.pos;
        // var secLexemObj = this._getByCapt(lexemsData, second.value);
        // Vue.set(app.subjectsList, app.pos, createSchemeSubject(lexemObj, subjName));
        // nodeInfo["src"] = '/images/' + lexemObj['image'];
        // app.pos++;
        // switch (op.value) {
        //     case '+' :
        //         name = schemeStack.addToStack(first.value);
        //         sys.addNode(name, nodeInfo);// add some nodes to the graph and watch it go...
        //         break;
        //     case ':' : break;
        //     case '*' : break;
        // }

    },


    _analyzeLexem : function(lexem) {

        var node = this._renderLexem(lexem,0, 0);

        console.log('node');
        console.log(node);
        var visualEntity = visualStack.createVisualElement(node);
        var prepared = visualStack.getPrepared();
        var op = visualStack.getPreparedOp();
        if (prepared && !op) {
            throw "пропущена операция между лексемами";
        }
        if(!prepared) {
            visualStack.prepare(visualEntity);
            return;
        }
        console.log('---prepared:');
        console.log(prepared);
        console.log('---ent:');
        console.log(visualEntity);
        console.log('---op:');
        console.log(op);
        var result = this._relateGroups(prepared, visualEntity, op);
        visualStack.prepare(result);
        visualStack.prepareOp(null); //remove operation
    },
    _openScope: function() {
        visualStack.resetPrepared();

    },
    _closeScope: function() {
        var node = visualStack.getFromStack();
        if (!node.length) {
            throw "Пропущено символ (";
        }
        var op = visualStack.getFromStackOp();
        if( op[0] && node[0]) {
            var result = this._relateGroups(node[0], visualStack.getPrepared(), op[0]);
            visualStack.prepare(result);

        }
    },

    _analyzeOp : function(op) {
        visualStack.prepareOp(op);
    },

    buildScheme: function (str, sys, app) {
        this._sys = sys;
        this._app = app;
        var startPos = 0;
        schemeStack.clearStack();
        while((startPos = this._getNext(schemeStack, str, startPos)) !==-1)
        {
            console.log("position:"+startPos);
            var literal = schemeStack.getLastAdded();
            console.log('literal:"'+literal.value+'"');
            if(literal.type == 'lexem') {

                this._analyzeLexem(literal.value);
                continue;
            }
            //else literal.type is 'op'
            switch(literal.value ) {
                case '(' :
                    this._openScope();
                    break;
                case ')' :
                    this._closeScope();
                    break;
                default:
                    this._analyzeOp(literal.value);
                    break;
            }
            // if(literal.type == 'lexem') {
            //     if(!firstGroup) {
            //         firstGroup = this._paintObj(literal, offset, 0);
            //     }
            //     else {
            //         secondGroup = this._paintObj(literal, offset+1, 0);
            //     }
            // }
            // if (literal.type == 'op') {
            //     op = literal;
            // }
            // if (firstGroup && secondGroup && op) {
            //     offset+=2;
            //     firstGroup = this._relateGroups( firstGroup, secondGroup, op)['right'];
            //     secondGroup = op = null;
            // }
        }
        if(!visualStack.checkEmptyStacks()) {
            throw "Пропущено символ )";
        }

    }
};
