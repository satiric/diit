/**
 * Created by decadal on 11.02.17.
 */


/**
 * functions for checking the symbols
 */
/**
 * @param sym
 * @returns {boolean}
 */
function isLitera(sym) {
    return (/[a-zA-Z]/.test(sym));
}

/**
 * @param ops
 * @param op
 * @returns {*}
 */
function isOp(ops, op) {
    return (ops)
        ? (ops.indexOf(op) !==-1)
        : null;
}

/**
 * @param ent
 * @param lexem
 * @returns {boolean}
 */
function isEntity(ent, lexem) {
    return (typeof ent[lexem] !== 'undefined');
}

/**
 * end block with functions for checking the symbols
 */