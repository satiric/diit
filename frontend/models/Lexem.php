<?php

namespace app\models;

use yii;

/**
 * This is the model class for table "lexems".
 *
 * @property integer $id
 * @property string $name
 * @property string $caption
 * @property string $image
 *
 * @property Attrs[] $attrs
 * @property Rules[] $rules
 */
class Lexem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lexems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_recive'], 'integer'],
            [['image'], 'string'],
            [['name'], 'string', 'max' => 25],
            [['caption'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'caption' => 'Caption',
            'image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrs()
    {
        return $this->hasMany(Attr::className(), ['id_lexem' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRules()
    {
        return $this->hasMany(Rule::className(), ['id_lexem' => 'id']);
    }
}
