<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 09.01.17
 * Time: 16:19
 */

namespace app\models;
    /**
     * This is the model class for table "files".
     *
     * @property integer $id
     * @property string $title
     * @property string $description
     * @property string $name
     * @property string $ext
     *
     */
class File extends \yii\base\Model
{
    /**
     * @param $file
     * @param $root
     * @return array|bool|int|null|string
     */
    public function uploadImageFile($file,$root, $name = null)
    {

//        if(is_array($file)) {
//            $rez = [];
//            for($i = 0, $size = count($file); $i < $size; $i++ ) {
//                try {
//                    $rez[] = $this->uploadImageFile($file[$i], $root, $getName);
//                }
//                catch (Exception $e) {
//                    $rez[] = null;
//                }
//            }
//            return $rez;
//        }
        $uploadForm = new ImageUploadForm();
        if(isset($file[0])) {
            $file = $file[0];
        }
        if(!($path = $uploadForm->upload($file,$root, $name)))
        {
            return false;
        };
        return $uploadForm->fileName. "." .$uploadForm->fileExt;
    }

    /**
     * @param $path
     * @return bool
     */
    public function delRealFile($path)
    {
        if(file_exists($path))
        {
            return unlink($path);
        }
        return false;
    }
}