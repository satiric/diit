<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attrs".
 *
 * @property integer $id
 * @property string $caption
 * @property string $symbol
 * @property double $min_value
 * @property double $max_value
 * @property integer $id_lexem
 *
 * @property Lexems $idLexem
 */
class Attr extends \yii\db\ActiveRecord
{
    const SCENARIO_UPDATE = 'update';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attrs';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_UPDATE => ['id','caption', 'symbol','id_lexem','min_value', 'max_value'],
            self::SCENARIO_DEFAULT => ['caption', 'symbol','id_lexem','min_value', 'max_value']
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['caption', 'id_lexem'], 'required'],
            [['min_value', 'max_value'], 'number'],
            [['id_lexem'], 'integer'],
            [['caption'], 'string', 'max' => 255],
            [['symbol'], 'string', 'max' => 50],
            [['id_lexem'], 'exist', 'skipOnError' => true, 'targetClass' => Lexem::className(), 'targetAttribute' => ['id_lexem' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'caption' => 'Caption',
            'symbol' => 'Symbol',
            'min_value' => 'Min Value',
            'max_value' => 'Max Value',
            'id_lexem' => 'Id Lexem',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLexem()
    {
        return $this->hasOne(Lexem::className(), ['id' => 'id_lexem']);
    }
}
