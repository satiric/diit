<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 09.01.17
 * Time: 16:20
 */

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class ImageUploadForm extends Model {


    /**
     * @var UploadedFile
     */
    public function rules()
    {
        return [
            [['fileObj'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, gif, jpg, bmp, jpeg'],
        ];
    }



    /**
     * @var \yii\web\UploadedFile|null
     */
    public $fileObj = null;
    public $fileName;
    public $fileExt;
    public $basePath = "";

    /**
     * @param $root string way to the physical location of the fie. TODO check for Project Path
     * @param $ext string file extension
     * @param bool $nameOnly
     * @return string path or filename with random name
     */
    public static function generFilePath($root,$ext,$nameOnly=false)
    {
        do {
            $name = md5(microtime() . rand(0, 9999));
            $filePath = $root . DIRECTORY_SEPARATOR. $name . "." .  $ext;
        }
        while (file_exists($filePath));
        return (!$nameOnly)
            ? $filePath
            : $name;
    }

    protected function getUploadedPath($root, $name = null)
    {
        if($name) {
            $this->fileName = 'scheme';
            $this->fileExt = 'png';
            return $root . DIRECTORY_SEPARATOR. "scheme.png";
        }
        
        $name = self::generFilePath($root,$this->fileObj->extension,true);
        $this->fileName = $name;
        $this->fileExt = $this->fileObj->extension;
        return $root . DIRECTORY_SEPARATOR. $name . "." . $this->fileExt;
    }

    private function imageCreateFromExt($intFormat,$filename)
    {
        switch($intFormat)
        {
            case 1:
                return imagecreatefromgif($filename);
                break;
            case 2:
                return imagecreatefromjpeg($filename);
                break;
            case 3:
                return imagecreatefrompng($filename);
                break;
            default:
                return null;
        }
    }

    private function saveImageFile($intFormat,$img,$path)
    {
        switch($intFormat)
        {
            case 1:
                imagegif($img, $path);
                break;
            case 2:
                imageconvolution($img, array( // улучшаем четкость
                    array(-1,-1,-1),
                    array(-1,16,-1),
                    array(-1,-1,-1) ),
                    8, 0);
                imagejpeg($img, $path);
                break;
            case 3:
                imagepng($img, $path);
                break;
            default:
                return false;
        }
        return true;
    }

    //@todo think about validating file size etc
    /**
     * @param $fileObject
     * @param $root
     * @return bool|string
     */
    public function upload($fileObject,$root, $name = null)
    {
        if (!$fileObject)
        {
            return false;
        }
        $this->fileObj = $fileObject;
        if (!$this->validate())
        {
            return false;
        }
        $filePath = $this->getUploadedPath($root, $name);
//        if (!$this->resizeImg(520,400,$filePath))  //this function is save file too
//        {
        $this->fileObj->saveAs($filePath);
//        }
        chmod($filePath,0644);
        return $filePath;
    }

    /**
     * @param $w
     * @param $h
     * @param $path
     * @return bool
     */
    private function resizeImg($w, $h, $path)
    {
        if(is_null($this->fileObj))
        {
            return false;
        }
        $filename = $this->fileObj->tempName;
        $size_img = getimagesize($filename);
        $realWidth = $size_img[0];
        $realHeight = $size_img[1];
        if (($realWidth<$w) && ($realHeight<$h))
        {
            return false;
        }

        $src_img = $this->imageCreateFromExt($size_img[2],$filename); // $size_img[2] - int type format
        if (!$src_img)
        {
            return false;
        }

        if($realWidth > $realHeight)
        {
            $koe = $realWidth/$w;
            $newHeight = ceil($realHeight/$koe);
            $img = imagecreatetruecolor($w, $newHeight);
            imagecopyresampled($img,$src_img,0,0,0,0,$w,$newHeight,imagesx($src_img),imagesy($src_img));
        }
        else
        {
            $koe = $realHeight/$h;
            $newWidth = ceil($realWidth/$koe);
            $img = imagecreatetruecolor($newWidth, $h);
            imagecopyresampled($img,$src_img,0,0,0,0,$newWidth,$h,imagesx($src_img),imagesy($src_img));
        }

        $rez = $this->saveImageFile($size_img[2],$img,$path);
        imagedestroy($img);
        imagedestroy($src_img);
        return $rez;
    }

}