<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rules".
 *
 * @property integer $id
 * @property string $body
 * @property integer $id_lexem
 *
 * @property Lexems $idLexem
 */
class Rule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rules';
    }


    public function saveByLexem($idLexem, $rulesArray) {
        if (is_array($rulesArray) && !empty($rulesArray)) {
            for ($i = 0, $size = count($rulesArray); $i < $size; $i++) {
                $body = $rulesArray[$i];
                if ($rule = Rule::find()->where(['id_lexem' => $idLexem])->andWhere(['position'=>$i])->one()) {
                    $rule->body = $body;
                    $rule->save();
                }
                else
                {
                    $rule = new Rule();
                    $rule->id_lexem = $idLexem;
                    $rule->body = $body;
                    $rule->position = $i;
                    $rule->save();
                }
            }
            $rules = Rule::find()->where(['id_lexem' => $idLexem])->andWhere('position > :index',[':index'=>$i-1])->all();
            for($i = 0, $size = count($rules); $i < $size; $i++) {
                $rules[$i]->delete();
            };
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_lexem'], 'required'],
            [['id_lexem', 'chance'], 'integer'],
            [['body'], 'string', 'max' => 255],
            [['id_lexem'], 'exist', 'skipOnError' => true, 'targetClass' => Lexem::className(), 'targetAttribute' => ['id_lexem' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'body' => 'Body',
            'id_lexem' => 'Id Lexem',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLexem()
    {
        return $this->hasOne(Lexem::className(), ['id' => 'id_lexem']);
    }
}
