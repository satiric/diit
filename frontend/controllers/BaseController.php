<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 09.05.17
 * Time: 16:01
 */

namespace frontend\controllers;

use \Yii;
use yii\web\Controller;

abstract class BaseController extends Controller
{
    /**
     * set format for ajax request, procedure
     */
    protected function returnFormatJson()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    }

    /**
     * @return bool|mixed
     */
    protected function isAjax() {
        return Yii::$app->request->isAjax;
    }

    /** return array with states like status and data, for json response
     * @param $status
     * @param array $data
     * @param null $code
     * @return array
     */
    protected function jsonResponseObj($status, $data = [], $code = null) {
        $this->returnFormatJson();
        return [
            'status' => $status,
            'code' => $code,
            'data' => $data
        ];
    }

    /** like jsonResponseObj, but with "error" binding
     * @param $msg
     * @param array $errors
     * @param string $status
     * @return array
     */
    protected function jsonBadResponseObj($msg, $errors = [], $status = "error") {
        return $this->jsonResponseObj($status, [
            'message' => $msg,
            'errors' => $errors
        ], 500);
    }


}