<?php

namespace frontend\controllers;

use app\models\Attr;
use app\models\File;
use app\models\Lexem;
use app\models\Rule;
use frontend\models\FileRecord;
use frontend\utils\RulesManager;
use frontend\utils\SchemeImageManager;
use \Yii;
use frontend\models\Project;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProjectsController implements the CRUD actions for Project model.
 */
class ProjectsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    protected function getPath($id) {
        return \Yii::getAlias('@webroot').'/uploads/'.$id;
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Project::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $path = $this->getPath($id);
        $notCached = \Yii::$app->request->get("notCached");
        if( ! $notCached) {
            $rules = (file_exists($path))
                ? (new RulesManager())->loadRules($id)
                :  $this->getActualRules();
        }
        else {
            $rules = $this->getActualRules();
        }
        $image = file_exists($path.'/scheme.png') ? Yii::getAlias('@web/uploads/'.$id.'/scheme.png') : '';
        return $this->render('view', [
            'model' => $this->findModel($id),
            'attrs' => Attr::findAll([]),
            'lexems' => $rules,
            'rules' => Rule::findAll([]),
            'schemeImage' => $image
        ]);
    }

    public function getActualRules() {
        return Lexem::find()
            ->with('rules')
            ->with('attrs')
            ->asArray()
            ->all() ;
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionBindScheme($id){
        if(!$project = $this->findModel($id)) {
            return $this->jsonBadResponseObj('model not found');
        };
        $project->formula = Yii::$app->request->post('formula');
        if (!$project->save()) {
            return  $this->jsonBadResponseObj("fail", $project->getErrors());
        }
        $rules = Lexem::find()->with('rules')->with('attrs')->asArray()->all();
       (new RulesManager())->saveRules($rules, $project->id);
       return $this->jsonResponseObj(200);
    }

    
    public function actionLoadFile($id)//@todo think about using tmp file
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fileRecord = new FileRecord();
        $fileRecord->name = 'scheme';
        $fileRecord->ext = 'png';
        $fileRecord->project_id = $id;
        $path = \Yii::getAlias('@webroot').'/uploads/'.$id;
        if(!file_exists($path)) {
            mkdir($path);
        }
        
        $fileObj = UploadedFile::getInstancesByName("inputsfile");
        $file = new File();
        $file->uploadImageFile($fileObj,$path, 'scheme.png');
        return $this->jsonResponseObj("success", ["result" => $fileRecord->save()]);
    }


    public function actionBindSchemeImage($id){
        if(!$project = $this->findModel($id)) {
            return $this->jsonBadResponseObj('model not found');
        };
        $file = Yii::$app->request->post('file');
        $project->attrs =  Yii::$app->request->post('collected');
        (new SchemeImageManager())->saveImage($file, $id);
        return($project->save())
            ? $this->jsonResponseObj(200)
            : $this->jsonBadResponseObj("fail", $project->getErrors());
    }
    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    public function actionSaveExperience($id) {
        if(!$project = $this->findModel($id)) {
            return $this->jsonBadResponseObj('model not found');
        };
        $exp = [
            'attrs' => json_decode(Yii::$app->request->post('attrs')),
            'solution' => Yii::$app->request->post('solution')
        ];

        $path = \Yii::getAlias('@webroot').'/uploads/'.$id;
        if(!file_exists($path)) {
            mkdir($path);
        }
        if(!file_exists($path.'/solutions.json')) {
            file_put_contents($path.'/solutions.json', json_encode(['sol' => [$exp]], JSON_UNESCAPED_UNICODE));
        }
        else {
            $sols = json_decode(file_get_contents($path.'/solutions.json'), true);
            $sols['sol'][] = $exp;
            file_put_contents($path.'/solutions.json', json_encode($sols,JSON_UNESCAPED_UNICODE));
        }
        return $this->jsonResponseObj('success');
    }
}
