<?php
namespace frontend\utils;
use frontend\models\FileRecord;

/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 11.05.17
 * Time: 8:19
 */
class SchemeImageManager
{

    public function saveImage($file, $toProjectId) {
        $fileRecord = new FileRecord();
        $fileRecord->name = 'scheme';
        $fileRecord->ext = 'png';
        $fileRecord->project_id = $toProjectId;
        $path = \Yii::getAlias('@webroot').'/uploads/'.$toProjectId;
        if(!file_exists($path)) {
            mkdir($path);
        }
        $file =  base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file));

        file_put_contents($path.'/scheme.png', $file, 777);
        chmod($path.'/scheme.png', 0777);
        $fileRecord->save();
    }

}