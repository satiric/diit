<?php
namespace frontend\utils;
use frontend\models\FileRecord;

/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 11.05.17
 * Time: 8:19
 */
class RulesManager
{
    
    protected function encodeRules($rules) {
        return json_encode($rules, JSON_UNESCAPED_UNICODE);
    }
    
    protected function decodeRules($rules) {
        return json_decode($rules, true);
    }
    
    public function saveRules($rules, $toProjectId) {
        $file = new FileRecord();
        $file->name = 'rules';
        $file->ext = 'json';
        $file->project_id = $toProjectId;
        $file->save();
        $path = \Yii::getAlias('@webroot').'/uploads/'.$toProjectId;
        if(!file_exists($path)) {
            mkdir($path);
        }
        file_put_contents($path.'/rules.json',  $this->encodeRules($rules));
    }
    
    public function loadRules($fromProjectId) {
        $path = \Yii::getAlias('@webroot').'/uploads/'.$fromProjectId.'/rules.json';
        if(!file_exists($path)) {
            throw new \Exception;
        }
        $content = file_get_contents($path);
        return $this->decodeRules($content);
    }

}