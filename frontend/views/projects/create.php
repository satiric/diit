<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Project */

$this->title = 'Створити проект';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
