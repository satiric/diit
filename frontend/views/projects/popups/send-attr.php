<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 30.01.17
 * Time: 9:25
 */
?>

<div class="modal fade" id="sendAttr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="loader"></div>
                <form>
                    <div class="form-group">
                        <label>Назва</label>
                        <input class="form-control" name="caption" :value="attr.caption" placeholder="Введіть назву">
                    </div>
                    <div class="form-group">
                        <label>Код</label>
                        <input class="form-control" name="symbol" :value="attr.symbol" placeholder="Введіть позначення">
                    </div>
                    <div class="form-group">
                        <label>Мінімум</label>
                        <input class="form-control" :value="attr.min_value" name="min_value">
                    </div>
                    <div class="form-group">
                        <label>Максимум</label>
                        <input class="form-control" :value="attr.max_value" name="max_value">
                    </div>
                    <div class="form-group">
                        <!--                        <label>Од. виміру</label>-->
                        <!--                        <input class="form-control" name="cache_period">-->
                    </div>
                    <input class="id-lexem" type="hidden" class="form-control" name="id_lexem" :value="attr.id_lexem">
                    <input class="id-attr" type="hidden" class="form-control" name="id" :value="attr.id">
                    <button type="submit" class="btn btn-success btn-sm">Зберігти</button>
                    <button class="btn btn-danger btn-sm closer" aria-haspopup="true" aria-expanded="false" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Відмінити
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
