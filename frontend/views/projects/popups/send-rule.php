<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 30.01.17
 * Time: 9:25
 */
?>

<div class="modal fade" id="sendRule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="loader"></div>
                <form>
                    <div class="form-group">
                        <label>Правило</label>
                        <input class="form-control" name="body" :value="rule.body" placeholder="Введіть правило">
                    </div>
                    <div class="form-group">
                        <label>Шанс</label>
                        <input class="form-control" :value="rule.chance" name="chance">
                    </div>

                    <input class="id-lexem" type="hidden" class="form-control" name="id_lexem" :value="rule.id_lexem">
                    <input class="id-rule" type="hidden" class="form-control" name="id" :value="rule.id">
                    <button type="submit" class="btn btn-success btn-sm">Зберігти</button>
                    <button class="btn btn-danger btn-sm closer" aria-haspopup="true" aria-expanded="false" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Відмінити
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
