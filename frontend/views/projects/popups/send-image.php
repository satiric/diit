<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 30.01.17
 * Time: 9:00
 */

?>

<div class="modal fade" id="sendImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <label class="control-label">Оберіть файл</label>
                <input id="input-44" name="inputsfile[]" type="file" multiple class="file-loading">
                <div id="errorBlock" class="help-block"></div>
            </div>
        </div>
    </div>
</div>
