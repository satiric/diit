<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 30.01.17
 * Time: 12:22
 */
?>


<div class="modal fade" id="sendLexem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="loader"></div>
                <p class="bg-info" style="padding:5px; -webkit-border-radius: 5px ;-moz-border-radius:5px ;border-radius: 5px;">При зміненні назви лексеми правила, до складу яких входить лексема, необхідно змінити відповідним чином, замінив стару назву лексеми новою</p>
                <form>
                    <div class="form-group">
                        <label>Назва</label>
                        <input class="form-control" name="name" :value="lexem.name" placeholder="Введіть назву">
                    </div>
                    <div class="form-group">
                        <label>Приймач?</label>
                        <input name="is_recive" v-if="lexem.is_recive != 0" type="checkbox" checked>
                        <input name="is_recive" v-if="lexem.is_recive == 0" type="checkbox">
                    </div>

                    <input class="id" type="hidden" class="form-control" name="id" :value="lexem.id">
                    <button type="submit" class="btn btn-success btn-sm">Зберігти</button>
                    <button class="btn btn-danger btn-sm closer" aria-haspopup="true" aria-expanded="false" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Відмінити
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
