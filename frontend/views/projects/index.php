<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проекти';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus "></span>', ['create'], ['class' => 'btn btn-success']) ?></h1>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => 'з {begin, number} по {end, number} (Всього: {totalCount, number})',
        'rowOptions' => function ($model, $index, $widget, $grid){
            $className = ($model->formula) ? 'success':'warning';
            return  ['class'=> $className];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'label'=> 'Створено',
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d H:m:s']
            ],
            [
                'label'=> 'Останні зміни',
                'attribute' => 'updated_at',
                'format' => ['date', 'php:Y-m-d H:m:s']
            ],
            [
                'label'=> 'Формула',
                'attribute' => 'formula',
                'content' => function($model) {
                    return ($model->formula) ? 'є' : 'немає';
                }
            ],
            [
                'label'=> 'Атрибути та схема',
                'attribute' => 'formula',
                'content' => function($model) {
                    return ($model->attrs) ? 'є' : 'немає';
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
