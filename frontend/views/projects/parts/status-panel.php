<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 30.01.17
 * Time: 10:44
 */
?>

<div class="col-md-4" id="statusPanel">
    <button id="genVals" class="btn btn-default" @click="generate()">Генерація</button> 
    
    <div :class="'panel panel-default ' + subject.name"  v-for="subject in subjectsList" v-if = "subject.name &&  subject.attrs.length">

        <div class="panel-body" v-if="subject.attrs" >
            <div v-for="attr in subject.attrs">
                <div class="progress" >
                    <!--:aria-valuenow="attr.value" :aria-valuemin="attr.min_value" :aria-valuemax="attr.max_value"  :style="'width:'+((attr.value/(attr.max_value - attr.min_value))*100)+'%' "-->
                    <div class="progress-bar progress-bar-success" role="progressbar"  style="width:100%">
                        <span>[{{ subject.name }}]  <b>{{attr.value}}</b> ({{attr.caption}})</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
            