<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 30.01.17
 * Time: 10:47
 */
?>
<p class="bg-info" style="padding:10px">Зауваження: при заповненні шансу остача розподілюється між іншими правилами, для яких не вказано шанс, порівну. Загальна верогідність дорівнює 100</p>
<label>Базове правило:</label>
<input type="text" class="form-control" value="">
<table id="logsTable" class="table table-hover">
    <thead>
    <tr>
        <th>Літерал</th>
        <th>Правила</th>
        <th>Приймач</th>
        <th>Зображення</th>
        <th>Керування</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for="lexem in lexems">
        <td> {{ lexem.name }}</td>
        <td>
            <p v-if="lexem.rules.length == 0">Терминал</p>
            <table v-if="lexem.rules.length != 0" class="table table-bordered">
                <thead>
                <th>Правило</th>
                <th>Шанс</th>
                <th></th>
                </thead>
                <tbody>
                <tr v-for="rule in lexem.rules">
                    <td> {{rule.body}}</td>
                    <td> {{rule.chance}} </td>
                    <td>
                        <button class="btn btn-primary btn-sm" aria-haspopup="true" v-on:click="setActive(rule)" aria-expanded="false" data-toggle="modal" data-target="#sendRule">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button class="btn btn-danger btn-sm remove-lexem" v-on:click="removeRule(rule.id)" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td>
            <p v-if="lexem.is_recive != 0 ">
                так
            </p>
            <p v-if="lexem.is_recive == 0">
                ні
            </p>
        </td>
        <td>
            <button class="btn btn-primary btn-sm" @click="loadImage(lexem.id)" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#sendImg">
                <span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
            </button><br /><br />
            <img v-if="lexem.image" :src="'/images/'+lexem.image" />
        </td>
        <td>
            <button class="btn btn-primary btn-sm add-rule" :data-id="lexem.id" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#sendRule">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </button>
            <button class="btn btn-primary btn-sm" v-on:click="setActiveLexem(lexem)" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#sendLexem">
                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </button>
            <button class="btn btn-danger btn-sm remove-attr" aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            </button>
        </td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="5">
            <button title="Додати лексему" class="btn btn-primary btn-lg addEntity" type="button">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </button>
        </td>
    </tr>
    </tfoot>
</table>