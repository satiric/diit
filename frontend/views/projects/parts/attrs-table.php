<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 30.01.17
 * Time: 10:52
 */
?>

<table id="attrsTable" class="table table-hover">
    <thead>
    <tr>
        <th>Літерал</th>
        <th>Атрибути</th>
        <th>Керування</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for="lexem in lexems">
        <td> {{ lexem.name }}</td>
        <td>
            <table class="table table-bordered">
                <thead>
                <th>Назва</th>
                <th>Код</th>
                <th>Мінімум</th>
                <th>Максимум</th>
                <th></th>
                </thead>
                <tbody>
                <tr v-for="attrib in lexem.attrs">
                    <td> {{attrib.caption}}</td>
                    <td> {{attrib.symbol}} </td>
                    <td> {{attrib.min_value}} </td>
                    <td> {{attrib.max_value}} </td>
                    <td>
                        <button class="btn btn-primary btn-sm" v-on:click="setActive(attrib.id, lexem.id)" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#sendAttr">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button class="btn btn-danger btn-sm remove-attr" v-on:click="removeAttr(attrib.id)" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td>
            <button class="btn btn-primary btn-sm edit-lexem" :data-id="lexem.id" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#sendAttr">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </button>
        </td>
    </tr>
    </tbody>
</table>
