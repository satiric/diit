<?php
/**
 * Created by PhpStorm.
 * User: decadal
 * Date: 30.01.17
 * Time: 10:44
 */
?>

<div class="col-md-1" id="workPanel">
    <form id = "solutions">
        <label>
            <input class="form-control" value="0"> % -> 0
        </label>
        <label v-for="subject in subjectsList" v-if="subject.is_recive == 1">
            <input class="form-control" :name="subject.name" value="0"> % {{ subject.name }}
        </label>
        <button type="button" @click="subm" class="btn btn-success">Прийняти</button>
    </form>
    <br>

</div>
