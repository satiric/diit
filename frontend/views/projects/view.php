<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Project */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title. " [ id: ".$model->id . "; створено ". date("Y-m-d H:i:s",$model->created_at)." ]";
$this->registerCss(".wrn { padding:10px; } .saveSchemeImage {display:none;}");
?>
<script>
    var selectedProject = <?=$model->id?>;
    var savedProjectAttrs = JSON.parse('<?=$model->attrs?>');
</script>
<div class="project-view">
    <div class="site-index">
        <div class="container-fluid">
            <div class="row">
    <h3><?= Html::encode($this->title) ?>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви впевнені що хочете видалити?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> Прийняті рішення', \yii\helpers\Url::base("http")."/uploads/".$model->id."/solutions.json", [
            'class' => 'btn btn-success',
        ]) ?>


    </h3>

    <!--div class="col-md-6">
        <?php //DetailView::widget([
//            'model' => $model,
//            'attributes' => [
//                'id',
//                [
//                    'attribute' => 'title',
//                    'label'=> 'Назва проекту',
//                ],
//                [
//                    'attribute' => 'created_at',
//                    'label'=> 'Створено',
//                    'format' => ['date', 'php:Y-m-d H:m:s']
//                ],
//                [
//                    'attribute' => 'updated_at',
//                    'label'=> 'Останні зміни',
//                    'format' => ['date', 'php:Y-m-d H:m:s']
//                ],
//
//            ],
//        ]) ?>
        </div-->

                </div>
            </div>
        </div>
    </div>


<?php

/* @var $this  \yii\web\View */
$this->title = 'Diit';
$this->registerJsFile('@web/js/arbor/lib/arbor.js',['depends'=>'frontend\assets\AppAsset']);

$this->registerJsFile('@web/js/core.js',['depends'=>'frontend\assets\AppAsset']);

$this->registerJsFile('@web/js/vue.js', ['position'=>\yii\web\View::POS_HEAD]);

$this->registerJsFile('@web/js/plugins/canvas-to-blob.min.js',['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/plugins/sortable.min.js',['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/plugins/purify.min.js',['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/fileinput.min.js',['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/locales/ru.js',['depends'=>'frontend\assets\AppAsset']);

$this->registerCssFile('@web/css/fileinput.min.css',['depends'=>'frontend\assets\AppAsset']);


?>

<script>
    var lexemArray = {};
    var lexemIds = {};
    var currentName = '';
    var lexemsData = {};
    <?php

    for ($i = 0, $size = count($lexems); $i < $size; $i++):?>
    <?php $current = $lexems[$i]; ?>
    lexemsData[<?=$current['id']?>] = JSON.parse('<?=json_encode($current)?>');
    currentName = '<?=$current['name']?>';
    lexemArray[currentName] = [];
    <?php for ($j = 0, $sizeRules = count($current['rules']); $j < $sizeRules; $j++):?>
    lexemArray[currentName].push('<?=$current['rules'][$j]['body']?>');
    <?php endfor;?>
    lexemIds[currentName] = <?=$lexems[$i]['id']?>;
    <?php endfor;
    ?>

</script>


<div class="site-index">
    <div class="container-fluid">
        <div class="row">
            <?=$this->render('parts/status-panel'); ?>
            <div class="col-md-9 col-md-offset-2" id="contentLogs">
                <ul class="nav nav-tabs" id="contentTabs" role="tablist" >
                    <li id="MetaSearchEngine" role="presentation"><a href="#mse" aria-controls="mse" role="tab" data-toggle="tab">Правила</a></li>
                    <li id="StateTabLink" role="presentation" ><a href="#mc" aria-controls="mc" role="tab" data-toggle="tab">Стан</a></li>
                    <li id="operations" role="presentation" ><a href="#ops" aria-controls="ops" role="tab" data-toggle="tab">Операції</a></li>
                    <li id="attribs" role="presentation" ><a href="#attrs" aria-controls="attrs" role="tab" data-toggle="tab">Атрибути</a></li>
                    <li id="Statistic" role="presentation"><a href="#statistic" aria-controls="statistic" role="tab" data-toggle="tab">Схема</a></li>
                </ul>
                <div class="loader"></div>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="mse">

                        <?=$this->render('parts/lexems-table');?>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="mc">
                        <pre><code class="language-json logs-result" id="stateResult"></code></pre>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="ops">
                        <table id="opsTable" class="table table-hover">
                            <thead>
                            <tr>
                                <th>Операція</th>
                                <th>Опис</th>
                                <th>Позначення на схемі</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr><td>+</td><td>Послідовне з'єднання</td><td></td></tr>
                            <tr><td>:</td><td>Паралельне з'єднання</td><td></td></tr>
                            <tr><td>*</td><td>Розгалудження</td><td></td></tr>
                            <tr><td>(</td><td>-</td><td>-</td></tr>
                            <tr><td>)</td><td>-</td><td>-</td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="attrs">

                        <?=$this->render('parts/attrs-table');?>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="statistic">
                     <?php if(!$schemeImage) : ?>

                        <?php if(!$model->formula) : ?>
                        <p class="bg-danger wrn">Уважно! При будуванні схеми формула, всі параметри та правила, які задані
                            на поточний момент, будуть збережені у файли, и подальша робота з проектом буде спиратись на ці файли.
                            Таким чином, після побудови схеми неможливо внести зміни до формули, за якою будувалась схема.
                            Для іншої схеми необхідно створити новий проект.</p>
                        <?php endif;?>
                        <div class="input-group">
                            <?php if($model->formula) : ?>
                                <p><?=$model->formula?></p>
                                <input type="hidden" class="form-control" id="schemeCode" value="<?=$model->formula?>">
                            <?php else:?>
                                <input type="text" class="form-control" id="schemeCode" value="">
                            <?php endif;?>

                            <span class="input-group-btn">
                                <button class="btn btn-primary buildScheme" type="button">
                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Побудувати
                                </button>
                                <?php if(!$model->formula) : ?>
                                <button class="btn btn-default bootstrapScheme" type="button">
                                    <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Розгорнути
                                </button>
                                <?php endif; ?>
                                 <button class="btn btn-default saveSchemeImage" type="button">
                                     <span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Зберігти схему
                                 </button>

                            </span>
                        </div>
                        <p id="errorScheme" class="text-danger"></p>
                        <canvas id="viewport" width="600" height="400"></canvas>
                        <?php else :?>
                         <p><?=$model->formula?></p>
                         <button data-pid="<?=$model->id?>" id="saveOtherSchemeImage" class="btn btn-default" type="button" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#sendImg">
                             <span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Інша схема...
                         </button>
                         <img id="scheme-image" src="<?=$schemeImage .'?'.time()?>" />
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <?=$this->render('parts/work-panel'); ?>
        </div>
    </div>
</div>

<?=$this->render('popups/send-attr'); ?>
<?=$this->render('popups/send-lexem'); ?>
<?=$this->render('popups/send-image'); ?>
<?=$this->render('popups/send-rule'); ?>
