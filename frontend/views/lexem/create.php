<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Lexem */

$this->title = 'Create Lexem';
$this->params['breadcrumbs'][] = ['label' => 'Lexems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lexem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
