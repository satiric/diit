<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\FileRecord */

$this->title = 'Create File Record';
$this->params['breadcrumbs'][] = ['label' => 'File Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-record-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
