<?php

/* @var $this  \yii\web\View */
$this->title = 'Diit';
$this->registerJsFile('@web/js/arbor/lib/arbor.js',['depends'=>'frontend\assets\AppAsset']);

$this->registerJsFile('@web/js/core.js',['depends'=>'frontend\assets\AppAsset']);

$this->registerJsFile('@web/js/vue.js', ['position'=>\yii\web\View::POS_HEAD]);

$this->registerJsFile('@web/js/plugins/canvas-to-blob.min.js',['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/plugins/sortable.min.js',['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/plugins/purify.min.js',['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/fileinput.min.js',['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/locales/ru.js',['depends'=>'frontend\assets\AppAsset']);

$this->registerCssFile('@web/css/fileinput.min.css',['depends'=>'frontend\assets\AppAsset']);


?>
<style>
    #create-proj {
        margin:10px;
    }
</style>
<pre><?php

?>
    </pre>
<script>
    var lexemArray = {};
    var lexemIds = {};
    var currentName = '';
    var lexemsData = {};
<?php

for ($i = 0, $size = count($lexems); $i < $size; $i++):?>
    <?php $current = $lexems[$i]; ?>
    lexemsData[<?=$current['id']?>] = JSON.parse('<?=json_encode($current)?>');
    currentName = '<?=$current['name']?>';
    lexemArray[currentName] = [];
    <?php for ($j = 0, $sizeRules = count($current['rules']); $j < $sizeRules; $j++):?>
    lexemArray[currentName].push('<?=$current['rules'][$j]['body']?>');
    <?php endfor;?>
    lexemIds[currentName] = <?=$lexems[$i]['id']?>;
<?php endfor;
?>

</script>


<div class="site-index">
    <div class="container-fluid">
        <div class="row">
            <?=$this->render('parts/status-panel'); ?>
            <div class="col-md-9 col-md-offset-2" id="contentLogs">
                <ul class="nav nav-tabs" id="contentTabs" role="tablist" >
                    <li id="Project" role="presentation" class="active"><a href="#project" aria-controls="mse" role="tab" data-toggle="tab">Проекти</a></li>
                    <li id="MetaSearchEngine" role="presentation"><a href="#mse" aria-controls="mse" role="tab" data-toggle="tab">Правила</a></li>
                    <li id="StateTabLink" role="presentation" ><a href="#mc" aria-controls="mc" role="tab" data-toggle="tab">Стан</a></li>
                    <li id="operations" role="presentation" ><a href="#ops" aria-controls="ops" role="tab" data-toggle="tab">Операції</a></li>
                    <li id="attribs" role="presentation" ><a href="#attrs" aria-controls="attrs" role="tab" data-toggle="tab">Атрибути</a></li>
                    <li id="Statistic" role="presentation"><a href="#statistic" aria-controls="statistic" role="tab" data-toggle="tab">Схема</a></li>
                </ul>
                <div class="loader"></div>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="project">
                        <div id="create-proj">
                            <?= \yii\helpers\Html::button('Додати проект', ['class' => 'btn btn-success', 'onclick' => 'location.href="/projects/create";']) ?>
                        </div>

                        <?= \yii\grid\GridView::widget([
                            'dataProvider' => new \yii\data\ActiveDataProvider([
                                'query' => \frontend\models\Project::find(),
                                'sort'=> ['defaultOrder' => ['created_at' => SORT_ASC]],
                                'pagination' => [
                                    'pageSize' => 20,
                                ],
                            ]),
                            'summary' => false,
                            'columns' => [
                                'title',
                                [
                                    'attribute' => 'created_at',
                                    'format' => ['date', 'php:Y-m-d H:m:s']
                                ],

                                [
                                    'headerOptions' => ['style' => 'width: 70px;'],
                                    'contentOptions' => ['style' => 'text-align: center'],
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{update} {delete}',
                                    'buttons' => [
                                        'update' => function($url, $model) {
                                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>',
                                                '/projects/update?id='.$model->id,
                                                ['title' => "Редагувати", 'data-pjax' => '0']);

                                        },
                                        'delete' => function($url, $model) {
                                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>',
                                                '/projects/delete?id='.$model->id,
                                                ['title' => Yii::t('yii', 'View'), 'data-pjax' => '1','data-method'=>'POST']);

                                        }
                                    ]
                                ],
                            ],
                        ]); ?>



                    </div>
                    <div role="tabpanel" class="tab-pane" id="mse">

                        <?=$this->render('parts/lexems-table');?>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="mc">
                        <pre><code class="language-json logs-result" id="stateResult"></code></pre>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="ops">
                        <table id="opsTable" class="table table-hover">
                            <thead>
                            <tr>
                                <th>Операція</th>
                                <th>Опис</th>
                                <th>Позначення на схемі</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr><td>+</td><td>Послідовне з'єднання</td><td></td></tr>
                            <tr><td>:</td><td>Паралельне з'єднання</td><td></td></tr>
                            <tr><td>*</td><td>Розгалудження</td><td></td></tr>
                            <tr><td>(</td><td>-</td><td>-</td></tr>
                            <tr><td>)</td><td>-</td><td>-</td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="attrs">

                        <?=$this->render('parts/attrs-table');?>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="statistic">
                        <div class="input-group">
                            <input type="text" class="form-control" id="schemeCode" value="">
                            <span class="input-group-btn">
                                <button class="btn btn-default buildScheme" type="button">
                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Виконати
                                </button>
                                <button class="btn btn-default bootstrapScheme" type="button">
                                    <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Розгорнути
                                </button>
                            </span>
                        </div>
                        <p id="errorScheme" class="text-danger"></p>
                        <canvas id="viewport" width="600" height="400"></canvas>
                    </div>
                </div>
            </div>
            <?=$this->render('parts/work-panel'); ?>
        </div>
    </div>
</div>

<?=$this->render('popups/send-attr'); ?>
<?=$this->render('popups/send-lexem'); ?>
<?=$this->render('popups/send-image'); ?>
<?=$this->render('popups/send-rule'); ?>
