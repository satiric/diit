<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attrs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attr-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Attr', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'caption',
            'symbol',
            'min_value',
            'max_value',
            // 'id_lexem',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
